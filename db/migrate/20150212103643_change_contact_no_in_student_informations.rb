class ChangeContactNoInStudentInformations < ActiveRecord::Migration
  def change
  	change_column :student_informations, :contact_no, :string
  end
end
