class CreateStudentInformations < ActiveRecord::Migration
  def change
    create_table :student_informations do |t|
      t.string :name
      t.string :father_name
      t.boolean :sex
      t.text :address
      t.string :email_id
      t.integer :contact_no

      t.timestamps
    end
  end
end
