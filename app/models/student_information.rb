class StudentInformation < ActiveRecord::Base
	validates :name , :father_name,:address, presence: true, length: {minimum: 5}
	#validates :sex , presence: true ,length: {minimum: 3}
	validates :email_id ,:contact_no, presence: true ,length: {minimum: 5}
end
