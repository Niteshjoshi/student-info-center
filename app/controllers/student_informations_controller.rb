class StudentInformationsController < ApplicationController
  def index
     @studentinfo=StudentInformation.all
  end

  def edit
    @studentinfo=StudentInformation.find(params[:id])
  end
  
  def new
      @studentinfo=StudentInformation.new
  end


  def create
  	@studentinfo=StudentInformation.new(student_params)
  	if @studentinfo.save
  	redirect_to action: :show, :id => @studentinfo.id
    else 
    render 'new'
    end  
  end

  def update
  @studentinfo = StudentInformation.find(params[:id])
 
    if @studentinfo.update(student_params)
    redirect_to student_informations_path(@studentinfo.id)
    else
    render 'edit'
    end
  end

  def search
  end

  def search_result
    @studentinfo=StudentInformation.find_by_name(params[:name])
    render 'search'
  end  
  
  def destroy
      @studentinfo=StudentInformation.find(params[:id])
      @studentinfo.destroy
      redirect_to :action => 'index'
  end

  def show
    @studentinfo=StudentInformation.find(params[:id])
  end

  private 
  
  def student_params
      params.require(:student_information).permit(:name,:father_name,:sex,:address,:email_id,:contact_no)
  end 
end